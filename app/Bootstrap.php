<?php

namespace z0s;

use Composer\Autoload\ClassLoader;
use League\Container\Container;
use League\Container\ReflectionContainer;

class Bootstrap
{
    public function __construct(
        protected ClassLoader $autoloader,
        public ?Container $container = null
    ) {
        $this->buildContainer();
    }

    public function buildContainer(): void
    {
        // Instantiate a new container if we're not given one
        $this->container = $this->container ?? new Container();

        // Register the ReflectionContainer so autowiring works
        $this->container->delegate(
            new ReflectionContainer()
        );

        // Add the autoloader into the container
        $this->container->add(ClassLoader::class, $this->autoloader);

        // Add the container itself to itself..
        $this->container->add(Container::class, $this->container);
    }

    public function getContainer(): Container
    {
        return $this->container;
    }
}
