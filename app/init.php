<?php

use z0s\Bootstrap;

$autoloaderPath = __DIR__ . '/../vendor/autoload.php';
if (!file_exists($autoloaderPath)) {
    throw new RuntimeException('You must run `composer install` to install the dependencies');
}

$autoloader = require $autoloaderPath;

// Bootstrap the system
return [
    new Bootstrap($autoloader),
    $autoloader
];
